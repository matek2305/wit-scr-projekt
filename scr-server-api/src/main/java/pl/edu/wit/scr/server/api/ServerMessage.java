package pl.edu.wit.scr.server.api;

/**
 * @author Mateusz Urbański <matek2305@gmail.com>
 */
public interface ServerMessage {

    String WELCOME = "WELCOME";
    String DISCONNECT = "DISCONNECT";
    String SPAWN_ME = "SPAWN_ME";
    String SPAWN_ENEMY = "SPAWN_ENEMY";
    String KILL_ME = "KILL_ME";
    String WORLD_MAP = "WORLD_MAP";
    String ROBOT_WINS = "ROBOT_WINS";
    String ENEMY_WINS = "ENEMY_WINS";
}
