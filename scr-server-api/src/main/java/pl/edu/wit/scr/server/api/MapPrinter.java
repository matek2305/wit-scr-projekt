package pl.edu.wit.scr.server.api;

/**
 * @author Mateusz Urbański <matek2305@gmail.com>
 */
public interface MapPrinter {

    void printMap(int[][] data);
}
