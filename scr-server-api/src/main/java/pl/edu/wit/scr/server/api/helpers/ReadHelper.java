package pl.edu.wit.scr.server.api.helpers;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * Created by Dominik on 2015-12-18.
 */
public final class ReadHelper {

    public static <T extends Serializable> T readObject(ObjectInputStream inputStream, Class<T> type) {
        try {
            return type.cast(inputStream.readObject());
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static String readMessage(ObjectInputStream inputStream) {
        try {
            return inputStream.readUTF();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ReadHelper() {
    }
}
