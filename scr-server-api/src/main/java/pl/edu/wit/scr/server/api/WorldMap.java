package pl.edu.wit.scr.server.api;

import java.io.Serializable;

/**
 * @author Mateusz Urbański <matek2305@gmail.com>
 */
public class WorldMap implements Serializable {

    public static final int DEAD_PLAYER = 4;
    public static final int ENEMY = 3;
    public static final int PLAYER = 2;
    public static final int OBSTACLE = 1;
    public static final int PATH = 0;

    private int[][] data;

    public WorldMap(int[][] data) {
        this.data = data;
    }

    public synchronized void print(MapPrinter printer) {
        printer.printMap(data);
    }

    public void spawn(SpawnPoint spawnPoint) {
        data[spawnPoint.getX()][spawnPoint.getY()] = PLAYER;
    }

    public void spawnEnemy(SpawnPoint spawnEnemy) {
        data[spawnEnemy.getX()][spawnEnemy.getY()] = ENEMY;
    }

    public void despawnEnemy(SpawnPoint spawnEnemy) { data[spawnEnemy.getX()][spawnEnemy.getY()] = PATH; }

    public void restorePath(SpawnPoint previousSpawnPoint) {
        data[previousSpawnPoint.getX()][previousSpawnPoint.getY()] = PATH;
    }

    public void markDeadPlayer() {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                if (data[i][j] == PLAYER) {
                    data[i][j] = DEAD_PLAYER;
                    return;
                }
            }
        }
    }

    public int[][] getData() {
        return data;
    }
}
