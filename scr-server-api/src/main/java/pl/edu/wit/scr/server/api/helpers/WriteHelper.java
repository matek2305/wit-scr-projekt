package pl.edu.wit.scr.server.api.helpers;

import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by Dominik on 2015-12-18.
 */
public final class WriteHelper {

    public static void writeMessage(ObjectOutputStream outputStream, String message) {
        try {
            outputStream.writeUTF(message);
            outputStream.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void writeObject(ObjectOutputStream outputStream, Object object) {
        try {
            outputStream.writeObject(object);
            outputStream.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private WriteHelper() {
    }
}
