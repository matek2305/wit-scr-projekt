package pl.edu.wit.scr.server.api;

import java.io.Serializable;

/**
 * @author Mateusz Urbański <matek2305@gmail.com>
 */
public class SpawnPoint implements Serializable {

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    private int x;
    private int y;

    public SpawnPoint(){
    }

    public SpawnPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpawnPoint that = (SpawnPoint) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return "SpawnPoint{x=" + x + ", y=" + y + "}";
    }
}
