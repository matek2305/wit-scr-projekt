package pl.edu.wit.scr.creature.move.strategy.graph;

import pl.edu.wit.scr.server.api.SpawnPoint;

/**
 * Created by Dominik on 2015-12-20.
 */
public class Vertex {

    // unikalny punkt na mapie przeszkod
    final SpawnPoint point;
    // znak reprezentujacy obiekt na mapie
    final int mapChar;

    public Vertex(SpawnPoint point, int mapChar) {
        this.point = point;
        this.mapChar = mapChar;
    }

    public SpawnPoint getPoint() {
        return point;
    }

    public int getMapChar() {
        return mapChar;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((point == null) ? 0 : point.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Vertex other = (Vertex) obj;
        if (point == null) {
            if (other.point != null)
                return false;
        } else if (!point.equals(other.point) || mapChar != other.mapChar)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return point.toString();
    }
}
