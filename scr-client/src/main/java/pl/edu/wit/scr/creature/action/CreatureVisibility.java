package pl.edu.wit.scr.creature.action;

/**
 * Created by Sylwia on 2016-01-13.
 */
public class CreatureVisibility {

    private int xStart;
    private int xEnd;
    private int yStart;
    private int yEnd;

    public int getyEnd() {
        return yEnd;
    }

    public void setyEnd(int yEnd) {
        this.yEnd = yEnd;
    }

    public int getxStart() {
        return xStart;
    }

    public void setxStart(int xStart) {
        this.xStart = xStart;
    }

    public int getxEnd() {
        return xEnd;
    }

    public void setxEnd(int xEnd) {
        this.xEnd = xEnd;
    }

    public int getyStart() {
        return yStart;
    }

    public void setyStart(int yStart) {
        this.yStart = yStart;
    }
}
