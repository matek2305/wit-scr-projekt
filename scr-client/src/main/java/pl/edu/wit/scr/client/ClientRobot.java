package pl.edu.wit.scr.client;

import pl.edu.wit.scr.creature.Creature;
import pl.edu.wit.scr.creature.RobotImpl;
import pl.edu.wit.scr.creature.move.strategy.DijkstryMoveImpl;
import pl.edu.wit.scr.server.api.ServerMessage;
import pl.edu.wit.scr.server.api.SpawnPoint;
import pl.edu.wit.scr.server.api.WorldMap;
import pl.edu.wit.scr.server.api.helpers.ReadHelper;
import pl.edu.wit.scr.server.api.helpers.WriteHelper;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * @author Mateusz Urbański <matek2305@gmail.com>
 */
class ClientRobot {

    private static final int PORT = 2343;

    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
        Socket socket = new Socket("localhost", PORT);
        System.out.println("connected");

        ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
        Creature robot = new RobotImpl(new SpawnPoint(0, 0), new SpawnPoint(14, 19));
        String serverMessage = null;

        initRobotWorldMap(serverMessage, robot, inputStream, outputStream);

        do {

            if(robotAction(serverMessage, inputStream, outputStream, robot)){
                break;
            }

        } while (!ServerMessage.DISCONNECT.equals(serverMessage));
    }

    /**
     * Akcja robota
     *
     * @param serverMessage
     * @param inputStream
     * @param outputStream
     * @param robot
     * @throws IOException
     * @throws InterruptedException
     * @throws ExecutionException
     */
    private static boolean robotAction(String serverMessage, ObjectInputStream inputStream, ObjectOutputStream outputStream, Creature robot)
            throws IOException, InterruptedException, ExecutionException {

        serverMessage = inputStream.readUTF();
        System.out.println("received: " + serverMessage);

        if (ServerMessage.WORLD_MAP.equals(serverMessage)) {

            robot.setWorldMap(ReadHelper.readObject(inputStream, WorldMap.class));

            if (sendMsgToServer(countMinPathTask(robot), outputStream, robot)) {
                return true;
            }

        } else if (ServerMessage.DISCONNECT.equals(serverMessage)) {
            return true;
        } else {
            throw new IllegalArgumentException("Not supported server message");
        }
        return false;
    }

    /**
     * Inicjalizacja kopi mapy swiata
     *
     * @param serverMessage
     * @param robot
     * @param inputStream
     * @param outputStream
     * @throws IOException
     */
    private static void initRobotWorldMap(String serverMessage, Creature robot, ObjectInputStream inputStream, ObjectOutputStream outputStream) throws IOException {
        // przywitanie, inicjalizacja mapy
        serverMessage = inputStream.readUTF();
        if (ServerMessage.WELCOME.equals(serverMessage)) {
            robot.setWorldMap(ReadHelper.readObject(inputStream, WorldMap.class));

            // wyslij punkt startowy
            outputStream.writeUTF(ServerMessage.SPAWN_ME);
            WriteHelper.writeObject(outputStream, robot.getStartPoint());
            System.out.println("start point: " + robot.getEndPoint());

        } else {
            throw new IllegalArgumentException("unsupported message");
        }

        assert robot.getWorldMap() != null;
    }

    /**
     * Uruchomienie obliczen najkrotszej sciezki w nowym watku
     *
     * @param robot
     */
    private static List<SpawnPoint> countMinPathTask(Creature robot) throws ExecutionException, InterruptedException {
        robot.setMoveStrategy(new DijkstryMoveImpl(robot.getStartPoint(), robot.getEndPoint(), robot.getWorldMap()));
        ExecutorService executor = Executors.newFixedThreadPool(1);
        FutureTask<List<SpawnPoint>> moveStrategyThread = new FutureTask<>(robot.getMoveStrategy());
        executor.execute(moveStrategyThread);
        while (!moveStrategyThread.isDone()) ;
        executor.shutdown();
        return moveStrategyThread.get();
    }

    /**
     * @param bestPath
     * @param outputStream
     * @param robot
     * @return czy przerwac dzialanie robota
     * @throws IOException
     */
    private static boolean sendMsgToServer(List<SpawnPoint> bestPath, ObjectOutputStream outputStream, Creature robot) throws IOException {
        // wyslij pierwszy punkt sciezki
        if (bestPath != null && bestPath.size() > 0) {
            robot.setStartPoint(bestPath.get(1));
            outputStream.writeUTF(ServerMessage.SPAWN_ME);
            WriteHelper.writeObject(outputStream, robot.getStartPoint());
            System.out.println("spawn point: " + robot.getStartPoint());
            return false;
        }

        // robot dotarl do celu
        outputStream.writeUTF(ServerMessage.ROBOT_WINS);
        System.out.println("Game over. Robot wins!");
        WriteHelper.writeObject(outputStream, robot.getEndPoint());
        return true;
    }
}
