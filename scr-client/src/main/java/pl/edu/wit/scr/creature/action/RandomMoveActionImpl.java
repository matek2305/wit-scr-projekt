package pl.edu.wit.scr.creature.action;

import pl.edu.wit.scr.server.api.SpawnPoint;
import pl.edu.wit.scr.server.api.WorldMap;

import java.util.Arrays;
import java.util.List;
import java.util.Random;


/**
 * Created by Sylwia on 2015-12-31.
 */
public class RandomMoveActionImpl {

    Random random = new Random();
    List<Integer> forbidden = Arrays.asList(1,2,3);

    public SpawnPoint randomSpawnPoint(WorldMap currentWorldMap) {
        SpawnPoint randomSpawnPoint = new SpawnPoint();

        boolean spawned = false;
        do {
            int x = random.nextInt(14);
            int y = random.nextInt(19);

            if (!forbidden.contains(currentWorldMap.getData()[x][y])) {
                randomSpawnPoint.setX(x);
                randomSpawnPoint.setY(y);
                spawned = true;
            }

        } while (!spawned);

        return randomSpawnPoint;
    }

    public SpawnPoint randomEndPoint(SpawnPoint currentPoint) {
        SpawnPoint randomEndPoint = new SpawnPoint();

        if (currentPoint.getX() < 7) {
            randomEndPoint.setX(14);
        } else {
            randomEndPoint.setX(0);
        }

        if (currentPoint.getY() < 10) {
            randomEndPoint.setY(currentPoint.getY()+5);
        } else {
            randomEndPoint.setY(currentPoint.getY()-5);
        }

        return randomEndPoint;
    }
}
