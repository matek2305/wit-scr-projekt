package pl.edu.wit.scr.creature;

/**
 * Created by Sylwia on 2016-01-13.
 */
public class Constants {

    public final static int ENEMY_VISIBILITY_RANGE = 1;

    public final static int MAP_X_BOUND = 14;
    public final static int MAP_Y_BOUND = 19;
    public final static int MAP_DUMMY_POINT = 19;

    public final static String MOVE_DOWN = "moveDown";
    public final static String MOVE_UP = "moveUp";
    public final static String MOVE_LEFT = "moveLeft";
    public final static String MOVE_RIGHT = "moveRight";
}
