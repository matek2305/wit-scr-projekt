package pl.edu.wit.scr.creature.action;

import pl.edu.wit.scr.creature.Constants;
import pl.edu.wit.scr.creature.EnemyImpl;
import pl.edu.wit.scr.server.api.SpawnPoint;
import pl.edu.wit.scr.server.api.WorldMap;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import static java.lang.Math.abs;

/**
 * Created by Sylwia on 2015-12-31.
 */
public class GoToTargetActionImpl {

    private EnemyImpl enemy = new EnemyImpl();
    List<Integer> forbidden = Arrays.asList(WorldMap.OBSTACLE, WorldMap.ENEMY);

    public SpawnPoint findRobot(WorldMap currentWorldMap, SpawnPoint currentPoint, SpawnPoint currentEndPoint) {
        SpawnPoint targetLocation = new SpawnPoint(Constants.MAP_DUMMY_POINT, Constants.MAP_DUMMY_POINT);
        CreatureVisibility creatureVisibility = enemy.creatureVisibility(currentPoint);

        for (int i = creatureVisibility.getxStart(); i < creatureVisibility.getxEnd(); i++) {
            for (int j = creatureVisibility.getyStart(); j < creatureVisibility.getyEnd(); j++) {
                if (currentWorldMap.getData()[i][j] == WorldMap.PLAYER) {
                    targetLocation.setX(i);
                    targetLocation.setY(j);
                }
            }
        }
        if (targetLocation.getX() == Constants.MAP_DUMMY_POINT || targetLocation.getY() == Constants.MAP_DUMMY_POINT) {
            targetLocation = currentEndPoint;
        }

        return targetLocation;
    }

    public SpawnPoint goToTarget(SpawnPoint currentPoint, SpawnPoint currentEndPoint, WorldMap currentWorldMap) {
        SpawnPoint moveTo = new SpawnPoint(Constants.MAP_DUMMY_POINT, Constants.MAP_DUMMY_POINT);

        int xDifference = currentPoint.getX();
        int yDifference = currentEndPoint.getY();

        String[] pref = new String[4];
        if (abs(xDifference) > abs(yDifference)) {
            if (currentPoint.getX() < currentEndPoint.getX()) {
                pref[0] = Constants.MOVE_RIGHT;
                if (currentPoint.getY() < currentEndPoint.getY()) {
                    pref[1] = Constants.MOVE_DOWN;
                    pref[2] = Constants.MOVE_UP;
                    pref[3] = Constants.MOVE_LEFT;
                } else {
                    pref[1] = Constants.MOVE_UP;
                    pref[2] = Constants.MOVE_DOWN;
                    pref[3] = Constants.MOVE_RIGHT;
                }
            } else {
                pref[0] = Constants.MOVE_LEFT;
                if (currentPoint.getY() < currentEndPoint.getY()) {
                    pref[1] = Constants.MOVE_DOWN;
                    pref[2] = Constants.MOVE_UP;
                    pref[3] = Constants.MOVE_RIGHT;
                } else {
                    pref[1] = Constants.MOVE_UP;
                    pref[2] = Constants.MOVE_DOWN;
                    pref[3] = Constants.MOVE_RIGHT;
                }
            }
        } else {

            if (currentPoint.getY() < currentEndPoint.getY()) {
                pref[0] = Constants.MOVE_DOWN;
                if (currentPoint.getX() < currentEndPoint.getX()) {
                    pref[1] = Constants.MOVE_RIGHT;
                    pref[2] = Constants.MOVE_LEFT;
                    pref[3] = Constants.MOVE_UP;
                } else {
                    pref[1] = Constants.MOVE_LEFT;
                    pref[2] = Constants.MOVE_RIGHT;
                    pref[3] = Constants.MOVE_UP;
                }
            } else {
                pref[0] = Constants.MOVE_UP;
                if (currentPoint.getX() < currentEndPoint.getX()) {
                    pref[1] = Constants.MOVE_RIGHT;
                    pref[2] = Constants.MOVE_LEFT;
                    pref[3] = Constants.MOVE_DOWN;
                } else {
                    pref[1] = Constants.MOVE_LEFT;
                    pref[3] = Constants.MOVE_RIGHT;
                }
            }
        }


        for (String s : pref) {
            if (moveTo.getX() == Constants.MAP_DUMMY_POINT || moveTo.getY() == Constants.MAP_DUMMY_POINT) {
                GoToTargetActionImpl thisClass = new GoToTargetActionImpl();
                try {
                    Method m = GoToTargetActionImpl.class.getMethod(s, WorldMap.class, SpawnPoint.class);
                    moveTo = (SpawnPoint) m.invoke(thisClass, currentWorldMap, currentPoint);
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        if (moveTo.getX() == Constants.MAP_DUMMY_POINT || moveTo.getY() == Constants.MAP_DUMMY_POINT) {
            moveTo = currentPoint;
        }
        return moveTo;
    }

    public SpawnPoint moveRight(WorldMap currentWorldMap, SpawnPoint currentPoint) {
        SpawnPoint moveTo = new SpawnPoint(Constants.MAP_DUMMY_POINT,Constants.MAP_DUMMY_POINT);
        try {
            if (!forbidden.contains(currentWorldMap.getData()[currentPoint.getX() + 1][currentPoint.getY()])) {
                moveTo.setX(currentPoint.getX() + 1);
                moveTo.setY(currentPoint.getY());
            }
        } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println(e.getMessage());
        }

        return moveTo;
    }

    public SpawnPoint moveLeft(WorldMap currentWorldMap, SpawnPoint currentPoint) {
        SpawnPoint moveTo = new SpawnPoint(Constants.MAP_DUMMY_POINT,Constants.MAP_DUMMY_POINT);
        try {
            if (!forbidden.contains(currentWorldMap.getData()[currentPoint.getX() - 1][currentPoint.getY()])) {
                moveTo.setX(currentPoint.getX() - 1);
                moveTo.setY(currentPoint.getY());
            }
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }

        return moveTo;
    }

    public SpawnPoint moveUp(WorldMap currentWorldMap, SpawnPoint currentPoint) {
        SpawnPoint moveTo = new SpawnPoint(Constants.MAP_DUMMY_POINT,Constants.MAP_DUMMY_POINT);
        try {
            if (!forbidden.contains(currentWorldMap.getData()[currentPoint.getX()][currentPoint.getY() - 1])) {
                moveTo.setX(currentPoint.getX());
                moveTo.setY(currentPoint.getY() - 1);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }

        return moveTo;
    }

    public SpawnPoint moveDown(WorldMap currentWorldMap, SpawnPoint currentPoint) {
        SpawnPoint moveTo = new SpawnPoint(Constants.MAP_DUMMY_POINT,Constants.MAP_DUMMY_POINT);
        try {
            if (!forbidden.contains(currentWorldMap.getData()[currentPoint.getX()][currentPoint.getY() + 1])) {
                moveTo.setX(currentPoint.getX());
                moveTo.setY(currentPoint.getY() + 1);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }

        return moveTo;
    }

}
