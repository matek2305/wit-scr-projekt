package pl.edu.wit.scr.creature;

import pl.edu.wit.scr.creature.move.strategy.MoveStrategy;
import pl.edu.wit.scr.server.api.SpawnPoint;
import pl.edu.wit.scr.server.api.WorldMap;

import java.io.Serializable;

/**
 * Istota
 * <p>
 * Created by Dominik on 2015-12-06.
 */
public interface Creature extends Serializable {

    /**
     * Uaktualnia mapa swiata istoty
     *
     * @param worldMap
     */
    void updateWorldMap(WorldMap worldMap);

    /**
     * @return SpawnPoint - aktualna pozycja istoty
     */
    SpawnPoint getSpawnPoint();

    /**
     * Ustawia aktualna pozycja istoty
     *
     * @param spawnPoint
     */
    void setSpawnPoint(SpawnPoint spawnPoint);

    /**
     * @return liczba calkowita - kwadrat widocznosci istoty
     */
    int getVisibilityRange();

    /**
     * Ustawia zakres widocznosci istoty
     *
     * @param range
     */
    void setVisibilityRange(int range);

    /**
     *
     * @return - punkt startowy
     */
    SpawnPoint getStartPoint();

    /**
     *
     * @param startPoint - punkt startowy
     */
    void setStartPoint(SpawnPoint startPoint);

    /**
     *
     * @return - punkt koncowy trasy
     */
    SpawnPoint getEndPoint();

    /**
     *
     * @param endPoint - punkt koncowy trasy
     */
    void setEndPoint(SpawnPoint endPoint);

    /**
     *
     * @return - strategia ruchu
     */
    MoveStrategy getMoveStrategy();

    /**
     *
     * @param moveStrategy - strategia ruchu
     */
    void setMoveStrategy(MoveStrategy moveStrategy);

    /**
     *
     * @return - swiat istoty
     */
    WorldMap getWorldMap();

    /**
     *
     * @param worldMap - swiat istoty
     */
    void setWorldMap(WorldMap worldMap);
}
