package pl.edu.wit.scr.creature.move.strategy;

import pl.edu.wit.scr.server.api.SpawnPoint;
import pl.edu.wit.scr.server.api.WorldMap;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Strategia wyliczania najkrutszej trasy z punktu A do B - strategy pattern
 * Obecnie wspieramy Dijkstry, w przyszlosci mozna by dodac algorytm A*
 * <p>
 * Created by Dominik on 2015-12-07.
 */
public interface MoveStrategy extends Callable<List<SpawnPoint>> {
    /**
     * Zwraca sekwencje pukktow najkrotszej sciezki (z pominieciem przeszkod) z punktu startPoint do endPoint dla mapy woldMap
     *
     * @param startPoint
     * @param endPoint
     * @param woldMap
     * @return
     */
    public List<SpawnPoint> getShortestPath(SpawnPoint startPoint, SpawnPoint endPoint, WorldMap woldMap);
}
