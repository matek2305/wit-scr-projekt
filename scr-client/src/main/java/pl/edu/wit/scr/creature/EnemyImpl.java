package pl.edu.wit.scr.creature;

import pl.edu.wit.scr.creature.action.CreatureVisibility;
import pl.edu.wit.scr.creature.move.strategy.MoveStrategy;
import pl.edu.wit.scr.server.api.SpawnPoint;
import pl.edu.wit.scr.server.api.WorldMap;

/**
 * Created by Dominik on 2015-12-06.
 */
public class EnemyImpl implements Creature {

    /**
     * Pozycja aktualna potworka
     */
    private SpawnPoint spawnPoint;

    /**
     * Bok kwadratu widocznosci potworka
     */
    private int visibilityRange = 0;

    /**
     * Kopia mapy serwera
     */
    private WorldMap worldMap;

    /**
     * Punkt na mapie, startowy potworka
     */
    private SpawnPoint startPoint;

    /**
     * Punkt na mapie, docelowy potworka
     */
    private SpawnPoint endPoint;

    /**
     * Strategia ruchu z punktu A do B
     */
    private MoveStrategy moveStrategy;

    @Override
    public void updateWorldMap(WorldMap worldMap) {
        this.worldMap = worldMap;
    }

    @Override
    public SpawnPoint getSpawnPoint() {
        return spawnPoint;
    }

    @Override
    public void setSpawnPoint(SpawnPoint spawnPoint) {
        this.spawnPoint = spawnPoint;
    }

    @Override
    public int getVisibilityRange() {
        return visibilityRange;
    }

    @Override
    public void setVisibilityRange(int range) {
        this.visibilityRange = range;
    }

    @Override
    public WorldMap getWorldMap() {
        return worldMap;
    }

    @Override
    public void setWorldMap(WorldMap worldMap) {
        this.worldMap = worldMap;
    }

    @Override
    public SpawnPoint getStartPoint() {
        return startPoint;
    }

    @Override
    public void setStartPoint(SpawnPoint startPoint) {
        this.startPoint = startPoint;
    }

    @Override
    public SpawnPoint getEndPoint() {
        return endPoint;
    }

    @Override
    public void setEndPoint(SpawnPoint endPoint) {
        this.endPoint = endPoint;
    }

    @Override
    public MoveStrategy getMoveStrategy() {
        return moveStrategy;
    }

    @Override
    public void setMoveStrategy(MoveStrategy moveStrategy) {
        this.moveStrategy = moveStrategy;
    }

    public CreatureVisibility creatureVisibility(SpawnPoint spawnPoint) {
        CreatureVisibility creatureVisibilityArea = new CreatureVisibility();

        if ((spawnPoint.getY() + Constants.ENEMY_VISIBILITY_RANGE) > Constants.MAP_Y_BOUND) {
            creatureVisibilityArea.setyEnd(Constants.MAP_Y_BOUND);
        } else {
            creatureVisibilityArea.setyEnd(spawnPoint.getY() + Constants.ENEMY_VISIBILITY_RANGE);
        }

        if ((spawnPoint.getX() + Constants.ENEMY_VISIBILITY_RANGE) > Constants.MAP_X_BOUND) {
            creatureVisibilityArea.setxEnd(Constants.MAP_X_BOUND);
        } else {
            creatureVisibilityArea.setxEnd(spawnPoint.getX() + Constants.ENEMY_VISIBILITY_RANGE);
        }

        if ((spawnPoint.getX() - Constants.ENEMY_VISIBILITY_RANGE) < 0) {
            creatureVisibilityArea.setxStart(0);
        } else {
            creatureVisibilityArea.setxStart(spawnPoint.getX() - Constants.ENEMY_VISIBILITY_RANGE);
        }

        if ((spawnPoint.getY() - Constants.ENEMY_VISIBILITY_RANGE) < 0) {
            creatureVisibilityArea.setyStart(0);
        } else {
            creatureVisibilityArea.setyStart(spawnPoint.getY() - Constants.ENEMY_VISIBILITY_RANGE);
        }

        return creatureVisibilityArea;
    }
}
