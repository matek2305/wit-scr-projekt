package pl.edu.wit.scr.creature;

import pl.edu.wit.scr.creature.move.strategy.MoveStrategy;
import pl.edu.wit.scr.server.api.SpawnPoint;
import pl.edu.wit.scr.server.api.WorldMap;

/**
 * Created by Dominik on 2015-12-06.
 */
public class RobotImpl implements Creature {

    /**
     * Pozycja aktualna robota
     */
    private SpawnPoint spawnPoint;

    /**
     * Bok kwadratu widocznosci robota
     */
    private int visibilityRange;

    /**
     * Kopia mapy serwera
     */
    private WorldMap worldMap;

    /**
     * Punkt na mapie, startowy robota
     */
    private SpawnPoint startPoint;

    /**
     * Punkt na mapie, docelowy robota
     */
    private SpawnPoint endPoint;

    /**
     * Strategia ruchu z punktu A do B
     */
    private MoveStrategy moveStrategy;

    public RobotImpl(SpawnPoint startPoint, SpawnPoint endPoint) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }

    public SpawnPoint getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(SpawnPoint startPoint) {
        this.startPoint = startPoint;
    }

    public SpawnPoint getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(SpawnPoint endPoint) {
        this.endPoint = endPoint;
    }

    @Override
    public WorldMap getWorldMap() {
        return worldMap;
    }

    @Override
    public void setWorldMap(WorldMap worldMap) {
        this.worldMap = worldMap;
    }

    @Override
    public void updateWorldMap(WorldMap worldMap) {
        this.worldMap = worldMap;
    }

    @Override
    public SpawnPoint getSpawnPoint() {
        return spawnPoint;
    }

    @Override
    public void setSpawnPoint(SpawnPoint spawnPoint) {
        this.spawnPoint = spawnPoint;
    }

    @Override
    public int getVisibilityRange() {
        return visibilityRange;
    }

    @Override
    public void setVisibilityRange(int range) {
        this.visibilityRange = visibilityRange;
    }

    @Override
    public MoveStrategy getMoveStrategy() {
        return moveStrategy;
    }

    @Override
    public void setMoveStrategy(MoveStrategy moveStrategy) {
        this.moveStrategy = moveStrategy;
    }
}
