package pl.edu.wit.scr.client;

import pl.edu.wit.scr.creature.action.GoToTargetActionImpl;
import pl.edu.wit.scr.creature.action.RandomMoveActionImpl;
import pl.edu.wit.scr.creature.move.strategy.DijkstryMoveImpl;
import pl.edu.wit.scr.server.api.ServerMessage;
import pl.edu.wit.scr.server.api.SpawnPoint;
import pl.edu.wit.scr.server.api.WorldMap;
import pl.edu.wit.scr.server.api.helpers.ReadHelper;
import pl.edu.wit.scr.server.api.helpers.WriteHelper;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

public class ClientEnemy {

    private static final int PORT = 2343;
    private final static int ENEMY_COUNT = 2;

    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(ENEMY_COUNT);

    public static void main(String[] args) {
        final Runnable runEmeny = () -> {

            try {
                Socket socket = new Socket("localhost", PORT);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());

                SpawnPoint startPoint;
                SpawnPoint endPoint;

                WorldMap worldMap;

                RandomMoveActionImpl randomMove = new RandomMoveActionImpl();
                GoToTargetActionImpl chaseTarget = new GoToTargetActionImpl();

                // przywitanie, inicjalizacja mapy
                String serverMessage = inputStream.readUTF();
                if (ServerMessage.WELCOME.equals(serverMessage)) {
                    worldMap = ReadHelper.readObject(inputStream, WorldMap.class);

                    startPoint = randomMove.randomSpawnPoint(worldMap);
                    endPoint = randomMove.randomEndPoint(startPoint);

                    outputStream.writeUTF(ServerMessage.SPAWN_ENEMY);
                    WriteHelper.writeObject(outputStream, startPoint);

                    System.out.println("Enemy start point: " + startPoint);
                } else {
                    throw new IllegalArgumentException("unsupported message");
                }

                assert worldMap != null;

                int help = 0;
                do {

                    help++;
                    serverMessage = inputStream.readUTF();

                    System.out.println("received: " + serverMessage);

                    if (ServerMessage.WORLD_MAP.equals(serverMessage)) {
                        WorldMap currentWorldMap = ReadHelper.readObject(inputStream, WorldMap.class);
                        SpawnPoint dummypoint = endPoint;
                        endPoint = chaseTarget.findRobot(currentWorldMap, startPoint, endPoint);

                        SpawnPoint nextPoint;
                        if (endPoint != dummypoint) {
                            System.out.println("Robot spotted at " + endPoint.getX() + " " + endPoint.getY());

                            DijkstryMoveImpl s = new DijkstryMoveImpl(startPoint, endPoint, worldMap);
                            List<SpawnPoint> bestPath = s.getShortestPath(startPoint, endPoint, worldMap);

                            // wyslij pierwszy punkt sciezki
                            if (bestPath != null && bestPath.size() > 0) {
                                nextPoint = bestPath.get(1);
                                startPoint = nextPoint;
                                outputStream.writeUTF(ServerMessage.SPAWN_ENEMY);
                                WriteHelper.writeObject(outputStream, nextPoint);
                                System.out.println("Enemy move point: " + nextPoint);
                                MILLISECONDS.sleep(500);
                            } else {
                                System.out.println("Game over - enemy wins!");
                                outputStream.writeUTF(ServerMessage.ENEMY_WINS);
                                WriteHelper.writeObject(outputStream, endPoint);
                                scheduler.shutdown();
                            }

                        } else {
                            nextPoint = chaseTarget.goToTarget(startPoint, endPoint, worldMap);
                            startPoint = nextPoint;
                            outputStream.writeUTF(ServerMessage.SPAWN_ENEMY);
                            WriteHelper.writeObject(outputStream, nextPoint);
                            System.out.println("Enemy move point: " + nextPoint);
                            MILLISECONDS.sleep(1000);
                        }
                    } else if (ServerMessage.DISCONNECT.equals(serverMessage)) {
                        scheduler.shutdown();
                    } else {
                        throw new IllegalArgumentException("Not supported server message");
                    }


                } while (!ServerMessage.DISCONNECT.equals(serverMessage) && help < 5);

                if (help == 5) {
                    System.out.println("Enemy despawns");
                    outputStream.writeUTF(ServerMessage.KILL_ME);
                    WriteHelper.writeObject(outputStream, startPoint);
                }

            } catch (IOException | InterruptedException e) {
                throw new RuntimeException(e);
            }
        };

        for (int enemies = 0; enemies < ENEMY_COUNT; enemies++) {
            scheduler.scheduleAtFixedRate(runEmeny, 0, 6, SECONDS);
        }
    }

}