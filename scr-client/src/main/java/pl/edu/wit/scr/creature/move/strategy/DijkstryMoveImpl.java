package pl.edu.wit.scr.creature.move.strategy;

import pl.edu.wit.scr.creature.move.strategy.graph.Edge;
import pl.edu.wit.scr.creature.move.strategy.graph.Vertex;
import pl.edu.wit.scr.server.api.SpawnPoint;
import pl.edu.wit.scr.server.api.WorldMap;

import java.util.*;
import java.util.concurrent.Callable;

/**
 * Wyliczanie Dijkstry - singleton pattern
 * Created by Dominik on 2015-12-07.
 */
public class DijkstryMoveImpl implements MoveStrategy, Callable<List<SpawnPoint>> {

    private List<Vertex> nodes;
    private List<Edge> edges;
    // znaleziono dla nich sciezke
    private Set<Vertex> settledNodes;
    // zbior ocenianych wierzcholkow
    private Set<Vertex> unSettledNodes;
    // (target, source) mapka dla zapamietania najktotszej sciezki
    private Map<Vertex, Vertex> predecessors;
    // dla kazdego wierzcholka przechowuje najkrotsza, obliczona odleglosc
    private Map<Vertex, Integer> distance;

    // najktotsza sciezka (zmienna krytyczna)
    final private SpawnPoint startPoint;
    final private SpawnPoint endPoint;
    final private WorldMap woldMapLock;

    public DijkstryMoveImpl(SpawnPoint startPoint, SpawnPoint endPoint, WorldMap woldMapLock) {
        this.nodes = new ArrayList<>();
        this.edges = new ArrayList<>();
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.woldMapLock = woldMapLock;
    }


    @Override
    public List<SpawnPoint> call() {
        List<SpawnPoint> bestPath;
        synchronized (woldMapLock){
            bestPath = getShortestPath(startPoint, endPoint, woldMapLock);
        }
        return bestPath;
    }

    @Override
    public synchronized List<SpawnPoint> getShortestPath(SpawnPoint startPoint, SpawnPoint endPoint, WorldMap woldMap) {
        createGraph(woldMap.getData());
        execute(nodes.get(20 * startPoint.getX() + startPoint.getY()));
        return getPointsPath(nodes.get(20 * endPoint.getX() + endPoint.getY()));
    }

    /**
     * Dijkstry from source
     *
     * @param source - wierzcholek zrodlowy
     */
    private void execute(Vertex source) {
        settledNodes = new HashSet<>();
        unSettledNodes = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<>();
        distance.put(source, 0);
        unSettledNodes.add(source);
        while (unSettledNodes.size() > 0) {
            Vertex node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            //unSettledNodes add, wyszukiwanie najkrotszej sciezki wsrod sasiadow wierzcholka node
            findMinimalDistances(node);
        }
    }

    /**
     * Wyszukiwanie najkrotszej sciezki do sasiadow wierzcholka node
     *
     * @param node - wierzcholek badany
     */
    private void findMinimalDistances(Vertex node) {
        List<Vertex> adjacentNodes = getNeighbors(node);
        for (Vertex target : adjacentNodes) {
            Integer shortestDistanceToTarget = getShortestDistance(node) + getDistance(node, target);
            if (getShortestDistance(target) > shortestDistanceToTarget && isNotObstacle(target)) {
                distance.put(target, shortestDistanceToTarget);
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }
    }

    /**
     * Pobranie odleglosci (wagi) przypisanej krawedzi pomiedzy node, target
     *
     * @param node - wierzcholek badany
     * @param target - wierzcholek docelowy
     * @return
     */
    private int getDistance(Vertex node, Vertex target) {
        for (Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && edge.getDestination().equals(target)) {
                return edge.getWeight();
            }
        }
        throw new RuntimeException("Should not happen");
    }

    /**
     * Wyszukiwanie sasiednich, niezatwierdzonych wierzcholkow wierzcho�ka node
     *
     * @param node - wierzcholek
     * @return
     */
    private List<Vertex> getNeighbors(Vertex node) {
        List<Vertex> neighbors = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && !isSettled(edge.getDestination()) && isNotObstacle(edge.getDestination())) {
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    /**
     * Wybor wiercholka o nalnmiejszej odleglosci ze zbioru vertexes
     *
     * @param vertexes - zbior wierzcholkow
     * @return
     */
    private  Vertex getMinimum(Set<Vertex> vertexes) {
        Vertex minimum = null;
        for (Vertex vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum) && isNotObstacle(vertex)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    /**
     * Juz zostala wyznaczona najkrotrza sciezka do tego wierzcholka
     *
     * @param vertex - sprawdzany wierzcholek
     * @return
     */
    private boolean isSettled(Vertex vertex) {
        return settledNodes.contains(vertex);
    }

    /**
     * Nie uwzgledniamy przeszkod
     *
     * @param vertex - sprawdzany wierzcholek
     * @return
     */
    private boolean isNotObstacle(Vertex vertex) {
        return vertex.getMapChar() == WorldMap.PATH;
    }

    /**
     * Pobranie odleglosci do wierzcholka destination
     *
     * @param destination - wierzolek docelowy
     * @return
     */
    private int getShortestDistance(Vertex destination) {
        return Optional.ofNullable(distance.get(destination)).orElse(Integer.MAX_VALUE);
    }

    /**
     * Zwraca najkrotrza sciezke z source do target (punkty)
     * NULL jescli nie ma sciezki
     */
    private LinkedList<SpawnPoint> getPointsPath(Vertex target) {
        LinkedList<SpawnPoint> path = new LinkedList<>();
        Vertex step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step.getPoint());
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step.getPoint());
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }


    /**
     * Stworz graf z tablicy dwuwymiarowej
     * Kazdy element tablicy jest wierzcholkiem
     * Odleglosc miedzy wierzcholkami rowna sie jeden
     *
     * @param data - mapka z przeszkodami
     * @return
     */
    private void createGraph(int data[][]) {

        for (int horizontal = 0; horizontal < data.length; horizontal++) {

            nodes.add(new Vertex(new SpawnPoint(horizontal, 0), data[horizontal][0]));

            if (horizontal > 0) {
                addEdge((horizontal - 1) * 20, horizontal * 20, 1);
                addEdge(horizontal * 20, (horizontal - 1) * 20, 1);
            }

            for (int vertical = 1; vertical < data[horizontal].length; vertical++) {
                nodes.add(new Vertex(new SpawnPoint(horizontal, vertical), data[horizontal][vertical]));

                addEdge((vertical - 1) + horizontal * 20, vertical + horizontal * 20, 1); // UWAGA odleglosc jest zawsze rowna jeden (NIEOPTYMALNE UPROSZCZENIE)
                addEdge(vertical + horizontal * 20, (vertical - 1) + horizontal * 20, 1);

                if (horizontal > 0) {
                    addEdge((vertical) + (horizontal - 1) * 20, vertical + (horizontal) * 20, 1);
                    addEdge(vertical + (horizontal) * 20, (vertical) + (horizontal - 1) * 20, 1);
                }

            }
        }
    }

    /**
     * Dodanie skierowanej krawedzi
     *
     * @param sourceLocNo - numer wierzcholka poczatkowego
     * @param destLocNo - numer wierzcholka koncowego
     * @param distance - waga
     */
    private void addEdge(int sourceLocNo, int destLocNo, int distance) {
        Edge lane = new Edge(nodes.get(sourceLocNo), nodes.get(destLocNo), distance);
        edges.add(lane);
    }

}