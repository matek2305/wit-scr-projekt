package pl.edu.wit.scr.creature.move.strategy.graph;

/**
 * Krawedz skierowana source -> destination
 * <p>
 * Created by Dominik on 2015-12-20.
 */
public class Edge {

    private final Vertex source;
    private final Vertex destination;
    private final int weight;

    public Edge(Vertex source, Vertex destination, int weight) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    public Vertex getDestination() {
        return destination;
    }

    public Vertex getSource() {
        return source;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return source + " " + destination;
    }
}
