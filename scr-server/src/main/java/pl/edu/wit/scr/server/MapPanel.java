package pl.edu.wit.scr.server;

import pl.edu.wit.scr.server.api.WorldMap;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.IOException;

/**
 * @author Mateusz Urbański <matek2305@gmail.com>
 */
class MapPanel extends JPanel {

    private static final int MAP_TILE_SIZE = 40;
    private static final int TARGET_X_POSITION = 19 * MAP_TILE_SIZE;
    private static final int TARGET_Y_POSITION = 14 * MAP_TILE_SIZE;

    private final Image robot;
    private final Image enemy;
    private final Image deadRobot;
    private final Image portal;

    private int[][] data;

    public MapPanel(int[][] data) throws IOException {
        this.robot = ImageIO.read(getClass().getResourceAsStream("/android.png"));
        this.enemy = ImageIO.read(getClass().getResourceAsStream("/vader.png"));
        this.deadRobot = ImageIO.read(getClass().getResourceAsStream("/death.png"));
        this.portal = ImageIO.read(getClass().getResourceAsStream("/portal.png"));
        this.data = data;
        setPreferredSize(new Dimension(800, 600));
    }

    public void update(int[][] data) {
        this.data = data;
    }

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());

        FontMetrics fontMetrics = g.getFontMetrics();
        Rectangle2D r = fontMetrics.getStringBounds("0", g);
        int x = (MAP_TILE_SIZE - (int) r.getWidth()) / 2;
        int y = (MAP_TILE_SIZE - (int) r.getHeight()) / 2 + fontMetrics.getAscent();

        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                g.drawString(String.valueOf(data[i][j]), j * MAP_TILE_SIZE + x, i * MAP_TILE_SIZE + y);
                g.drawImage(portal, TARGET_X_POSITION, TARGET_Y_POSITION, MAP_TILE_SIZE, MAP_TILE_SIZE, null);

                if (data[i][j] == WorldMap.OBSTACLE) {
                    g.fillRect(j * MAP_TILE_SIZE, i * MAP_TILE_SIZE, MAP_TILE_SIZE, MAP_TILE_SIZE);
                } else if (data[i][j] == WorldMap.PLAYER) {
                    g.drawImage(robot, j * MAP_TILE_SIZE, i * MAP_TILE_SIZE, MAP_TILE_SIZE, MAP_TILE_SIZE, null);
                } else if (data[i][j] == WorldMap.ENEMY) {
                    g.drawImage(enemy, j * MAP_TILE_SIZE, i * MAP_TILE_SIZE, MAP_TILE_SIZE, MAP_TILE_SIZE, null);
                } else if (data[i][j] == WorldMap.DEAD_PLAYER) {
                    g.drawImage(deadRobot, j * MAP_TILE_SIZE, i * MAP_TILE_SIZE, MAP_TILE_SIZE, MAP_TILE_SIZE, null);
                }
            }
        }
    }
}
