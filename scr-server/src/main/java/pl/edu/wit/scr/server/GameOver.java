package pl.edu.wit.scr.server;

/**
 * Created by Sylwia on 2016-01-16.
 */
public enum GameOver {
    INSTANCE;

    private boolean over = false;

    synchronized public boolean isOver() {
        return over;
    }

    synchronized public void setOver(boolean over) {
        this.over = over;
    }

    GameOver() {

    }
}
