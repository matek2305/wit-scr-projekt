package pl.edu.wit.scr.server;

import pl.edu.wit.scr.server.api.MapPrinter;
import pl.edu.wit.scr.server.api.WorldMap;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import static javax.swing.JOptionPane.showMessageDialog;

/**
 * @author Mateusz Urbański <matek2305@gmail.com>
 */
class Server extends JFrame implements MapPrinter {

    private static final int PORT = 2343;

    public static void main(String[] args) throws IOException {

        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Server is running");

            int[][] data = readFile("/map.txt");
            WorldMap worldMap = new WorldMap(data);
            Server server = new Server(new MapPanel(data));
            server.setVisible(true);

            while (!GameOver.INSTANCE.isOver()) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client connected");

                ClientHandler clientHandler = new ClientHandler(server, clientSocket, worldMap);
                clientHandler.start();
            }
            worldMap.print(server);
        }
    }

    private final MapPanel mapPanel;

    public Server(MapPanel mapPanel) {
        this.mapPanel = mapPanel;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        add(mapPanel);
        pack();
    }

    @Override
    synchronized public void printMap(int[][] data) {
        mapPanel.update(data);
        repaint();
        revalidate();
    }

    synchronized public void finish(boolean robotReachedTarget) {
        showMessageDialog(this, "Koniec symulacji, " + (robotReachedTarget ? "robot dotarł do celu i jest bezpieczny." : "robot nie dotarł do celu"));
        setVisible(false);
        dispose();
        System.exit(0);
    }

    private static int[][] readFile(String fileName) throws IOException {
        int[][] data = new int[15][20];

        InputStream inputStream = Server.class.getResourceAsStream(fileName);
        Scanner scanner = new Scanner(inputStream);

        int i = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            data[i++] = parse(line);
        }

        return data;
    }

    private static int[] parse(String line) {
        assert line.length() == 20;
        int[] parsed = new int[20];
        for (int i = 0; i < 20; i++) {
            parsed[i] = Character.getNumericValue(line.charAt(i));
        }
        return parsed;
    }
}
