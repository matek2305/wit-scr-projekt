package pl.edu.wit.scr.server;

import pl.edu.wit.scr.server.api.ServerMessage;
import pl.edu.wit.scr.server.api.SpawnPoint;
import pl.edu.wit.scr.server.api.WorldMap;
import pl.edu.wit.scr.server.api.helpers.ReadHelper;
import pl.edu.wit.scr.server.api.helpers.WriteHelper;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import static pl.edu.wit.scr.server.api.helpers.ReadHelper.readMessage;

/**
 * @author Mateusz Urbański <matek2305@gmail.com>
 */
class ClientHandler extends Thread {

    private final Server server;
    private final Socket socket;
    private final WorldMap worldMap;

    private final ObjectOutputStream outputStream;
    private final ObjectInputStream inputStream;

    public ClientHandler(Server server, Socket socket, WorldMap worldMap) throws IOException {
        this.server = server;
        this.socket = socket;
        this.worldMap = worldMap;

        this.outputStream = new ObjectOutputStream(socket.getOutputStream());
        this.inputStream = new ObjectInputStream(socket.getInputStream());
    }

    @Override
    public void run() {
        WriteHelper.writeMessage(outputStream, ServerMessage.WELCOME);
        WriteHelper.writeObject(outputStream, worldMap);
        SpawnPoint previousPlayerPoint = null;
        SpawnPoint previousEnemyPoint = null;

        while (!socket.isClosed()) {
            if (!GameOver.INSTANCE.isOver()) {
                String message = readMessage(inputStream);
                if (ServerMessage.SPAWN_ME.equals(message)) {
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }

                    SpawnPoint spawnPoint = ReadHelper.readObject(inputStream, SpawnPoint.class);
                    // nie pierwszy punkt
                    if (previousPlayerPoint != null) {
                        worldMap.restorePath(previousPlayerPoint);
                    }
                    if (!GameOver.INSTANCE.isOver()) {
                        System.out.println("spawn point: " + spawnPoint);
                        worldMap.spawn(spawnPoint);
                        previousPlayerPoint = spawnPoint;
                        worldMap.print(server);
                        WriteHelper.writeMessage(outputStream, ServerMessage.WORLD_MAP);
                        WriteHelper.writeObject(outputStream, worldMap);
                    } else {
                        WriteHelper.writeMessage(outputStream, ServerMessage.DISCONNECT);
                    }

                } else if (ServerMessage.SPAWN_ENEMY.equals(message)) {

                    SpawnPoint spawnPoint = ReadHelper.readObject(inputStream, SpawnPoint.class);
                    // nie pierwszy punkt
                    if (previousEnemyPoint != null) {
                        worldMap.restorePath(previousEnemyPoint);
                    }
                    if (!GameOver.INSTANCE.isOver()) {
                        System.out.println("Enemy spawn point: " + spawnPoint);
                        worldMap.spawnEnemy(spawnPoint);
                        worldMap.print(server);
                        previousEnemyPoint = spawnPoint;
                        WriteHelper.writeMessage(outputStream, ServerMessage.WORLD_MAP);
                        WriteHelper.writeObject(outputStream, worldMap);
                    } else {
                        WriteHelper.writeMessage(outputStream, ServerMessage.DISCONNECT);
                    }

                } else if (ServerMessage.KILL_ME.equals(message)) {
                    SpawnPoint despawnPoint = ReadHelper.readObject(inputStream, SpawnPoint.class);
                    System.out.println("despawning creature : " + despawnPoint);
                    worldMap.despawnEnemy(despawnPoint);
                    worldMap.print(server);
                    try {
                        socket.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                } else if (ServerMessage.ROBOT_WINS.equals(message)) {
                    GameOver.INSTANCE.setOver(true);
                    System.out.println("Game over. Robot wins");
                    WriteHelper.writeMessage(outputStream, ServerMessage.DISCONNECT);
                    finish(ServerMessage.ROBOT_WINS.equals(message));
                } else if (ServerMessage.ENEMY_WINS.equals(message)) {
                    GameOver.INSTANCE.setOver(true);
                    worldMap.markDeadPlayer();
                    worldMap.print(server);
                    System.out.println("Game over. Enemy wins");
                    WriteHelper.writeMessage(outputStream, ServerMessage.DISCONNECT);
                    finish(ServerMessage.ROBOT_WINS.equals(message));
                } else {
                    throw new IllegalArgumentException("unsupported message " + message);
                }
            } else {
                WriteHelper.writeMessage(outputStream, ServerMessage.DISCONNECT);
                break;
            }
        }
    }

    synchronized private void finish(boolean robotReachedTarget) {
        server.finish(robotReachedTarget);

        try {
            socket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
